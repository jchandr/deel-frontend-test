Deel Front-End Questionnaire

1. What is the difference between Component and PureComponent? Give an example where it might break my app.
	Purecomponents are high performance and do not re-render for every change unless the change is different from the current. Purecomponent does only shallow comparison, so we wont know if the props has actually changed. In this can we might have to use forceUpdate()

2. Context + ShouldComponentUpdate might be dangerous. Why is that? 
	It could stop the context propagation from parent to children. 

3. Describe 3 ways to pass information from a component to its PARENT. 
Props
Contexts
State management like Redux

4. Give 2 ways to prevent components from re-rendering. 
useMemo
useCallback

5. What is a fragment and why do we need it? Give an example where it might break my app. 
	Fragments can be used to render child component(s) without introducing a parent node to the virtual dom. 

6. Give 3 examples of the HOC pattern. 
Redux
If we want to use theming, we can use this pattern.
Callbacks

7. What's the difference in handling exceptions in promises, callbacks and async…await? 
In async await, we handle using try catch block
In promises, we use catch function
In callback, we can use try catch block

8. How many arguments does setState take and why is it async. 

Setstate takes 2 arguments. First is the object that has the values of new state variables. Second is the callback function that gets executed once the setstate is processed successfully. 
React batches multiple setstate calls to give better performance. If this is not async and having in mind that this is single threaded, it would make other parts of the application slower and sometimes unresponsive.

9. List the steps needed to migrate a Class to Function Component. 

change the component from class definition to function definition
Copy the contents of the render function and move it to the return of the component. 
Refactor the state definitions inside constructor and add as class object using useState hooks
Remove the constructor. To use the props, use destruct in the function parameter of the component. 
Check the lifecycle methods. Use useEffect hook whenever necessary. 

10. List a few ways styles can be used with components. 

Through style prop passing the style on camelCasing
As js object pass through style prop.
Through className prop passing the className. The css has to be exposed to this component before using the css classes.

11. How to render an HTML string coming from the server
	We can use dangerouslysetinnerHTML. 
