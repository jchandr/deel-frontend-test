import React from 'react';
import AutoComplete from './components/autoComplete/autoComplete';

function App() {
  return (
    <div style={{
      width: 300,
      marginLeft: 50
    }}>
      <AutoComplete onSelect={(d: Object) => {
        console.log(d)
      }}></AutoComplete>
    </div>
  );
}

export default App;
