import React, { MouseEvent, useCallback, useEffect, useRef, useState } from 'react';
import './autoComplete.css'
import OutClickDetect from '../../utils/checkOutsideClick';

type inputValue = string
type autoCompleteData = {
  name: string
}

const data = [
  {
    name: 'jeya jeyaraj',
  },{
    name: 'balaji',
  },{
    name: 'shalini',
  },{
    name: 'saadhana',
  },{
    name: 'chandrasekaran',
  },{
    name: 'saadhana jeyabalaji',
  },{
    name: 'shalini jeyabalaji',
  },{
    name: 'the quick brown fox jumps over the lazy frog. it go it. the quick brown fox jumps over the lazy frog. it go it',
  },
]

const AutoComplete : React.FC<{
  onSelect:Function
}> = ({
  onSelect
}) => {
  const wrapperRef = useRef(null)
  const [inputValue, setInputValue] = useState<inputValue>('')
  const [isDropdownShown, setIsDropDownShown] = useState(false)

  const [allData, setAllData] = useState<autoCompleteData[]>([])
  const [filteredData, setFilteredData] = useState<autoCompleteData[]>([])
  
  OutClickDetect(wrapperRef, () => {
    setIsDropDownShown(false)
  })


  const filterData = useCallback(async () => {
    if (inputValue.trim() === '') {
      setFilteredData(data)
      return
    }
    let valueToFilter = inputValue.trim().toLowerCase()
    const tempFilteredData = allData.filter(({name}) => name.trim().toLowerCase().includes(valueToFilter))
   
    setFilteredData(tempFilteredData)
  }, [inputValue, allData])

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {value} = e.target
    setInputValue(value)
    setIsDropDownShown(true)
  }

  const handleSuggestionClick = (e:MouseEvent, d:autoCompleteData) => {
    e.preventDefault()
    setInputValue(d.name)
    setIsDropDownShown(false)
    onSelect(d)
  }

  useEffect(() => {
    filterData()
  }, [inputValue, filterData])

  useEffect(() => {
    setAllData(data)
    setFilteredData(data)
  }, [])

  const buildHighlightedText:React.FC = (hay: any) => {
    const parts = hay.split(new RegExp(`(${inputValue})`, 'gi'));
    return (
      <span>
      { 
        parts.map((part:String, i:number) => {
          return (
            <span key={i} className={part.toLowerCase() === inputValue.toLowerCase() ? 'highlighted-text': ''}>
              { part }
            </span>
          )
        })
      }
    </span>
    );
  }

  return (
    <div className='Autocomplete' ref={wrapperRef}>
      <div className='Input-group'>
        <input className='Search-text' value={inputValue} type='text' onChange={(e) => handleInputChange(e)}></input>
        <button onClick={() => setInputValue('')} className='clear-selection'>x</button>
        <button className='Arrow-wrapper' onClick={() => setIsDropDownShown(!isDropdownShown)}>
          <i className={isDropdownShown ? 'Arrow Arrow-Up' : 'Arrow Arrow-Down'} />
        </button>
      </div>
      {
        isDropdownShown && (
          <div className='suggested-autocomplete-wrapper'>
            {
              filteredData.length > 0 ? (
              <>
              {
                filteredData.map((d, i) => {
                  return (
                    <button onClick={(e) => handleSuggestionClick(e, d)} className='suggestion-wrapper' key={i}>
                      {
                        buildHighlightedText(d.name)
                      }
                    </button>
                  )
                })
              }
              </>
              ) : (
                <button className='suggestion-wrapper no-match' >No Match Found</button>
              )
            }
          </div>
        )
      }
    </div>
  );
}



export default AutoComplete;
